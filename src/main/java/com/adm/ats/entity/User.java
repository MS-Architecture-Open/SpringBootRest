package com.adm.ats.entity;

import java.util.Objects;

public class User {

    private long id;
    private String nombre;
    private String apellido;
    private int edad;
    private String cargo;
    private double salario;

    public User(){
        id=0;
    }

    public User(long id, String nombre, String apellido, int edad, String cargo, double salario) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.cargo = cargo;
        this.salario = salario;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getId() == user.getId() &&
                getEdad() == user.getEdad() &&
                Double.compare(user.getSalario(), getSalario()) == 0 &&
                Objects.equals(getNombre(), user.getNombre()) &&
                Objects.equals(getApellido(), user.getApellido()) &&
                Objects.equals(getCargo(), user.getCargo());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getNombre(), getApellido(), getEdad(), getCargo(), getSalario());
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", edad=" + edad +
                ", cargo='" + cargo + '\'' +
                ", salario=" + salario +
                '}';
    }
}
