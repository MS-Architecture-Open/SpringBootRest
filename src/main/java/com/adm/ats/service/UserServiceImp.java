package com.adm.ats.service;

import com.adm.ats.Util.Util;
import com.adm.ats.entity.User;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Service("userService")
public class UserServiceImp implements UserService  {

    private static final AtomicLong counter = new AtomicLong();
    private static List<User> users;

    static{
        users= populateDummyUsers();
    }


    @Override
    public User findById(long id) {
        for(User user : users){
            if(user.getId() == id){
                return user;
            }
        }
        return null;
    }

    @Override
    public User findByName(String name) {
        for(User user : users){
            if(user.getNombre().equalsIgnoreCase(name)){
                return user;
            }
        }
        return null;
    }

    @Override
    public void saveUser(User user) {
        user.setId(counter.incrementAndGet());
        users.add(user);
    }

    @Override
    public void updateUser(User user) {
        int index = users.indexOf(user);
        users.set(index, user);
    }

    @Override
    public void deleteUserById(long id) {
        for (Iterator<User> iterator = users.iterator(); iterator.hasNext(); ) {
            User user = iterator.next();
            if (user.getId() == id) {
                iterator.remove();
            }
        }

    }

    @Override
    public List<User> findAllUsers() {
        return users;
    }

    @Override
    public void deleteAllUsers() {
        users.clear();
    }


    @Override
    public boolean isUserExist(User user) {
        return findByName(user.getNombre())!=null;
    }

    private static List<User> populateDummyUsers(){
        List<User> users = new ArrayList<User>();
        users.add(new User(counter.incrementAndGet(),"Alberto","Gonzales",30,"analista programado", 25000));
        users.add(new User(counter.incrementAndGet(),"Tom","Fabian", 32,"analista Funcional",28000));
        users.add(new User(counter.incrementAndGet(),"Jerome","Fernandez", 35,"Analista Organico",30000));
        users.add(new User(counter.incrementAndGet(),"Silvia","Perez", 40,"Arquitecto",360000));
        return users;
    }

}
