package com.adm.ats.service;

import com.adm.ats.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.List;



public interface UserService {

    User findById(long id);

    User findByName(String name);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUserById(long id);

    List<User> findAllUsers();

    void deleteAllUsers();

    boolean isUserExist(User user);


}

